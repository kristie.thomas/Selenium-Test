describe('Protractor Demo App', function() {
  it('should have a title', function() {
    browser.get('http://hello/');
    browser.getPageSource().then(function(body) { console.log(body); });
    expect(browser.getTitle()).toEqual('Hello World!');
  });
});
