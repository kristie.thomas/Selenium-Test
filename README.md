This is an example project that's trying to test a built artifact with selenium and I'm not sure what hostnames to give the selenium container to resolve the artifact under test.

For this example I'm using a sample hello world apache container as my built artifact.

I want to be able to start my web container and use selenium to run tests against it.  I've written an example test that should load the web page and confirm that the title is 'Hello world!'

The problem ends up being that the container running the job can resolve the web container but selenium has no information about it. I am hoping I don't have to parse out from /etc/hosts on the job container to deliver the ip to selenium with browser.get('http://Some-IP-instead-of-hostname/'). It would be simplest if there was a hosts entry added to the selenium container.

